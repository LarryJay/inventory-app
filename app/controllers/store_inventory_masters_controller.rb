class StoreInventoryMastersController < ApplicationController
  before_action :set_store_inventory_master, only: [:show, :edit, :update, :destroy]

  # GET /store_inventory_masters
  # GET /store_inventory_masters.json
  def index
    #  @count = StoreInventoryMaster.where(:quantity).count
    logger.info "@med_count---#{@med_count}"

    
    @store_inventory_masters = StoreInventoryMaster.paginate(:page => 1, :per_page => 2)
  end

  # GET /store_inventory_masters/1
  # GET /store_inventory_masters/1.json
  def show
  end

  # GET /store_inventory_masters/new
  def new
    @store_inventory_master = StoreInventoryMaster.new
  end

  # GET /store_inventory_masters/1/edit
  def edit
  end

  # POST /store_inventory_masters
  # POST /store_inventory_masters.json
  def create
    @store_inventory_master = StoreInventoryMaster.new(store_inventory_master_params)

    respond_to do |format|
      if @store_inventory_master.save
        format.html { redirect_to root_path, notice: 'Store inventory master was successfully created.' }
        format.json { render :show, status: :created, location: @store_inventory_master }
      else
        format.html { render :new }
        format.json { render json: @store_inventory_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /store_inventory_masters/1
  # PATCH/PUT /store_inventory_masters/1.json
  def update
    respond_to do |format|
      if @store_inventory_master.update(store_inventory_master_params)
        format.html { redirect_to root_path, notice: 'Store inventory master was successfully updated.' }
        format.json { render :show, status: :ok, location: @store_inventory_master }
      else
        format.html { render :edit }
        format.json { render json: @store_inventory_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /store_inventory_masters/1
  # DELETE /store_inventory_masters/1.json
  def destroy
    @store_inventory_master.destroy
    respond_to do |format|
      format.html { redirect_to store_inventory_masters_url, notice: 'Store inventory master was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_store_inventory_master
      @store_inventory_master = StoreInventoryMaster.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def store_inventory_master_params
      params.require(:store_inventory_master).permit(:id,:store_inventory_categories_id,:temp_quantity, :item_name, :cost_price, :selling_price, :manufaturer_name, :user_id, :quantity, :item_location)
    end
end
