class HomeController < ApplicationController

	def dispensing
		@dispensary = Dispensary.new
		@assigned_requests = AssignedRequest.joins("INNER JOIN store_requests ON store_requests.id = assigned_requests.store_request_id INNER JOIN store_inventory_categories ON store_inventory_categories.id = assigned_requests.store_inventory_categories_id INNER JOIN store_inventory_masters ON store_inventory_masters.id = assigned_requests.store_inventory_master_id ")
		.select("assigned_requests.id,  store_requests.user_id as requesting_user, store_request_id, store_requests.quantity as requested_quantity, assigned_requests.comments,store_inventory_masters.quantity as store_quantity_available, store_inventory_categories.name as category_name, item_name, cost_price, selling_price, manufaturer_name,item_location, date_requested,
				 store_requests.status as request_status,assigned_requests.store_manager_id as assigning_manager, assigned_requests.date_assigned as assigned_date, assigned_requests.status as assigned_status, assigned_requests.quantity as assigned_quantity")
				 .paginate(:page=>params[:page],per_page:7).order('id desc')

		@dispensaries = Dispensary.joins("INNER JOIN store_inventory_masters ON store_inventory_masters.id = dispensaries.store_inventory_master_id
		  INNER JOIN store_inventory_categories ON store_inventory_categories.id = dispensaries.store_inventory_categories_id
		  INNER JOIN assigned_requests ON assigned_requests.id = dispensaries.assigned_requests_id").select(" dispensaries.id, dispensaries.comments as dispensary_comments, dispensaries.date_dispensed as dispensed_date,
		  dispensaries.user_id as dispensary_user, dispensaries.quantity as dispensed_quantity, dispensaries.status as dispensary_status, dispensaries.store_inventory_master_id as item_inventory_id, dispensaries.store_inventory_categories_id as dispensed_category_id,
		  store_inventory_masters.item_name as dispensary_item_name, store_inventory_masters.cost_price as dispensary_item_cost_price, store_inventory_masters.selling_price as dispensary_item_selling_price,
		  store_inventory_masters.manufaturer_name as dispensed_item_manufacturer, store_inventory_masters.id as store_inventory_item_id, store_inventory_categories.name as category_name").paginate(:page=>params[:page],per_page:12).order('id desc')
	end

 	def index

		@patients = Patient.all

		# @users = User.all(:conditions => ["created_at >= ?", Date.today.at_beginning_of_month])
		@patient_stats = Patient.all.count

		@lastday = Patient.where( 'created_at >= :one_days_ago', :one_days_ago => 1.days.ago,).count


		@date_start = DateTime.now
		@date_end = @date_start - 24.hour
		@dailyDoctor = AssignPatient.where(:created_at => @date_end..@date_start , :role_id => 3).count
		@dailyappointments = Appointment.where(:created_at => @date_end..@date_start ).count

		#@dailyDoctor = AssignPatient.where( 'created_at >= :one_days_ago, role_id' ,:one_days_ago => 1.days.ago , :role_id => 3).count
	  #	@dailyDoctor = AssignPatient.where(role_id: 3, created_at >=  1.days.ago ).count

		logger.info "SEE THE STATS #{@patient_stats}"
		logger.info "SEE THE STATS lastday #{@lastday}"
		logger.info "SEE THE STATS dailyDoctor #{@dailyDoctor}"
		logger.info "SEE THE STATS dailyappointments #{@dailyappointments}"

		#   @store_requests = StoreRequest.all
		@store_inventory_masters = StoreInventoryMaster.where("quantity < ?", 6).paginate(:page=>params[:page],per_page:10).order('id desc')


		@store_inventory_masters.each do |total|


		# @count = StoreInventoryMaster.where( quantity: @quantity_in_stock)
		# logger.info "@med_count---#{@count}"
		# @count = params[:store_inventory_masters][:quantity]
		# logger.info "@total_count---#{@count}"

		@total_stock = StoreInventoryMaster.sum(:quantity)



		logger.info "@total_stock---#{@total_stock}"

		# join two tables together using the inner join method
		@store_inventory_masters = StoreInventoryMaster.joins("Inner join store_inventory_categories on store_inventory_categories.id =store_inventory_masters.store_inventory_categories_id")
		.select("store_inventory_masters.id,name,item_name,cost_price,selling_price,manufaturer_name,user_id,quantity,item_location").paginate(:page=>params[:page],per_page:10).order('id desc')


		@store_requests = StoreRequest.all
		@low_stock = StoreInventoryMaster.where("quantity < ?", 6)

		@low_stock.each do |total|

		total.quantity
		logger.info "@total_count---#{total.quantity}"

		logger.info "@total_count---#{total.item_name}"

		flash.now[:notice] = 'low stock ' + total.item_name

		respond_to do |format|
			format.html
		end

 	   end
      end

	end

	def confirm_dispense
		@new_id = params[:id]
		@dispensary = Dispensary.new
		respond_to do |format|
			format.js
		end
	end

 	@@count=2

 	def requests

      @store_request = StoreRequest.new
      @assigned_request = AssignedRequest.new

    	#inner joins for request table with inventory categories and inventory masters table
          @show_categories = StoreInventoryCategory.all

        @store_requests = StoreRequest.joins("INNER JOIN store_inventory_categories ON store_inventory_categories.id = store_requests.store_inventory_categories_id INNER JOIN store_inventory_masters ON store_inventory_masters.id = store_requests.store_inventory_master_id ")
        .select("store_inventory_master_id,store_requests.id, store_requests.store_inventory_categories_id as category_id, temp_quantity, store_requests.user_id as requesting_user, store_requests.quantity as requested_quantity, comments, store_inventory_masters.quantity as store_quantity_available, store_inventory_categories.name as category_name
           , item_name, cost_price, selling_price, manufaturer_name,item_location, date_requested ,store_requests.status").limit(@@count).order('id desc')

      @@count+=2
      respond_to do |format|
       format.html
       format.js
       #end
      end

      if params[:category]
        @store_requests = StoreRequest.joins("INNER JOIN store_inventory_categories ON store_inventory_categories.id = store_requests.store_inventory_categories_id INNER JOIN store_inventory_masters ON store_inventory_masters.id = store_requests.store_inventory_master_id ")
        .select("store_inventory_master_id,store_requests.id, store_requests.store_inventory_categories_id as category_id, temp_quantity, store_requests.user_id as requesting_user, store_requests.quantity as requested_quantity, comments, store_inventory_masters.quantity as store_quantity_available, store_inventory_categories.name as category_name
           , item_name, cost_price, selling_price, manufaturer_name,item_location, date_requested ,store_requests.status,store_inventory_categories.name as category_name").where(["store_requests.store_inventory_categories_id = ?","#{params[:category]}"]).paginate(:page=>params[:page],per_page:10)

        puts  "name found--------hjkl "
          else
        puts "not found"

      end

		# @multiple_requests_id =  params[:store_request][:id]

		# logger.info ("Store Req Id --- #{@multiple_requests_id}")

 	end


	def assigned_requests
		@show_categories = StoreInventoryCategory.all
		@new_id = params[:id]
		@dispensary = Dispensary.new
		@store_inventory_masters = StoreInventoryMaster.all

		@assigned_requests = AssignedRequest.joins("INNER JOIN store_requests ON store_requests.id = assigned_requests.store_request_id INNER JOIN store_inventory_categories ON store_inventory_categories.id = assigned_requests.store_inventory_categories_id INNER JOIN store_inventory_masters ON store_inventory_masters.id = assigned_requests.store_inventory_master_id ")
		.select("assigned_requests.id, store_requests.user_id as requesting_user, store_request_id, store_requests.quantity as requested_quantity, assigned_requests.comments,store_inventory_masters.quantity as store_quantity_available, store_inventory_categories.name as category_name, item_name, cost_price, selling_price, manufaturer_name,item_location, date_requested,
				store_requests.status as request_status, store_requests.store_manager_id as request_to_store_manager, assigned_requests.store_manager_id as assigned_by_store_manager, assigned_requests.date_assigned as assigned_date, assigned_requests.status as assigned_status, assigned_requests.quantity as assigned_quantity")
				.paginate(:page=>params[:page],per_page:6).order('id desc')

		if params[:category]
		@assigned_requests = AssignedRequest.joins("INNER JOIN store_requests ON store_requests.id = assigned_requests.store_request_id INNER JOIN store_inventory_categories ON store_inventory_categories.id = assigned_requests.store_inventory_categories_id INNER JOIN store_inventory_masters ON store_inventory_masters.id = assigned_requests.store_inventory_master_id ")
		.select("assigned_requests.id, store_requests.user_id as requesting_user, store_request_id, store_requests.quantity as requested_quantity, assigned_requests.comments,store_inventory_masters.quantity as store_quantity_available, store_inventory_categories.name as category_name, item_name, cost_price, selling_price, manufaturer_name,item_location, date_requested,
				store_requests.status as request_status, assigned_requests.store_manager_id as assigning_manager, assigned_requests.date_assigned as assigned_date, assigned_requests.status as assigned_status, assigned_requests.quantity as assigned_quantity").where(["store_requests.store_inventory_categories_id = ?","#{params[:category]}"])
				.paginate(:page=>params[:page],per_page:6).order('id desc')
				puts  "name found--------hjkl "
		else
			puts "not found"
		end
	end


	def receipts
		#inner joins table for ourchase receipts and store purchases
		@purchase_receipts = PurchaseReceipt.joins("INNER JOIN store_purchases ON store_purchases.id = purchase_receipts.store_purchase_id")
		.select("purchase_receipts.id, image, status, store_purchase_id, comments, user_id, item_name, cost_price, quantity, date_of_purchase").paginate(:page=>params[:page],per_page:5).order('id desc')

	end


	def item_purchase

		@store_purchases=StorePurchase.joins("Inner join store_inventory_categories on store_inventory_categories.id =store_purchases.store_inventory_categories_id").select("store_purchases.id,status,item_name,cost_price,date_of_purchase,quantity,name").paginate(:page=>params[:page],per_page:3).order('id desc')

	end

end
