class PurchaseReceiptsController < ApplicationController
  before_action :set_purchase_receipt, only: [:show, :edit, :update, :destroy]

  # GET /purchase_receipts
  # GET /purchase_receipts.json
  def index
    @purchase_receipts = PurchaseReceipt.all


    @purchase_receipts = PurchaseReceipt.joins("INNER JOIN store_purchases ON store_purchases.id = purchase_receipts.store_purchase_id")
      .select("purchase_receipts.id, image, user_id,comments, item_name, quantity, cost_price, date_of_purchase, store_purchase_id, status" ).order('id desc')
  end

  # GET /purchase_receipts/1
  # GET /purchase_receipts/1.json
  def show
  end

  # GET /purchase_receipts/new
  def new
    @purchase_receipt = PurchaseReceipt.new
  end

  # GET /purchase_receipts/1/edit
  def edit
  end

  # POST /purchase_receipts
  # POST /purchase_receipts.json
  def create
    @purchase_receipt = PurchaseReceipt.new(purchase_receipt_params)

    respond_to do |format|
      if @purchase_receipt.save
        format.html { redirect_to receipts_path, notice: 'Purchase receipt was successfully created.' }
        format.json { render :show, status: :created, location: @purchase_receipt }
      else
        format.html { render :new }
        format.json { render json: @purchase_receipt.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /purchase_receipts/1
  # PATCH/PUT /purchase_receipts/1.json
  def update
    respond_to do |format|
      if @purchase_receipt.update(purchase_receipt_params)
        format.html { redirect_to receipts_path, notice: 'Purchase receipt was successfully updated.' }
        format.json { render :show, status: :ok, location: @purchase_receipt }
      else
        format.html { render :edit }
        format.json { render json: @purchase_receipt.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchase_receipts/1
  # DELETE /purchase_receipts/1.json
  def destroy
    @purchase_receipt.destroy
    respond_to do |format|
      format.html { redirect_to purchase_receipts_url, notice: 'Purchase receipt was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_purchase_receipt
      @purchase_receipt = PurchaseReceipt.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def purchase_receipt_params
      params.require(:purchase_receipt).permit(:store_purchase_id, :image, :user_id, :status, :comments, :attachments)
    end
end
