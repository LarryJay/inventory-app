class StorePurchasesController < ApplicationController
  before_action :set_store_purchase, only: [:show, :edit, :update, :destroy]

  # GET /store_purchases
  # GET /store_purchases.json
  def index
    @store_purchases = StorePurchase.all
  @store_purchases=StorePurchase.joins("Inner join store_inventory_categories on store_inventory_categories.id =store_purchases.store_inventory_categories_id").select("store_purchases.id,status,item_name,cost_price,date_of_purchase,quantity,name").order('id desc')
  end

  # GET /store_purchases/1
  # GET /store_purchases/1.json
  def show
  end

  # GET /store_purchases/new
  def new
    @store_purchase = StorePurchase.new
  end

  # GET /store_purchases/1/edit
  def edit
  end

  # POST /store_purchases
  # POST /store_purchases.json
  def create
    @store_purchase = StorePurchase.new(store_purchase_params)

    respond_to do |format|
      if @store_purchase.save
        format.html { redirect_to item_purchase_path, notice: 'Store purchase was successfully created.' }
        format.json { render :show, status: :created, location: @store_purchase }
      else
        format.html { render :new }
        format.json { render json: @store_purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /store_purchases/1
  # PATCH/PUT /store_purchases/1.json
  def update
    respond_to do |format|
      if @store_purchase.update(store_purchase_params)
        format.html { redirect_to @store_purchase, notice: 'Store purchase was successfully updated.' }
        format.json { render :show, status: :ok, location: @store_purchase }
      else
        format.html { render :edit }
        format.json { render json: @store_purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /store_purchases/1
  # DELETE /store_purchases/1.json
  def destroy
    @store_purchase.destroy
    respond_to do |format|
      format.html { redirect_to store_purchases_url, notice: 'Store purchase was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_store_purchase
      @store_purchase = StorePurchase.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def store_purchase_params
      params.require(:store_purchase).permit(:store_inventory_categories_id, :item_name, :cost_price, :quantity, :date_of_purchase)
    end
end
