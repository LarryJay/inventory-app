class DispensariesController < ApplicationController
  before_action :set_dispensary, only: [:show, :edit, :update, :destroy]

  # GET /dispensaries
  # GET /dispensaries.json
  def index
    @assigned_request = AssignedRequest.all
    @dispensaries = Dispensary.all
    @dispensary = Dispensary.joins("INNER JOIN assigned_requests on assigned_requests.id = dispensaries.assigned_requests_id")
      .select("dispensaries.id, assigned_requests_id,  dispensaries.status as dispensary_status, dispensaries.comments as dispensary_comments, date_dispensed ").order('id desc')
  end


  # GET /dispensaries/1
  # GET /dispensaries/1.json
  def show
  end

  # GET /dispensaries/new
  def new
    @dispensary = Dispensary.new
  end

  # GET /dispensaries/1/edit
  def edit
  end

  # POST /dispensaries
  # POST /dispensaries.json
  def create
    @assigned_request = AssignedRequest.all
    @checked_value = params[:check_box][:id]
    logger.info "checked_value ---#{@checked_value}"

    @dispensary_items_save = []

    @dispensed_quantity = params[:dispensary][:quantity]
    logger.info "dispensed_quantity ---#{@dispensed_quantity}"

    # @dispensary = Dispensary.new(dispensary_params)
    val = 0;
    @checked_value.each do |values|
      
      @param_obj = AssignedRequest.find(values)
      logger.info ("Param_obj --- #{@param_obj.inspect}")

      logger.info ("Param_obj --- #{@param_obj.quantity}")

    




      @dispensary_items_save << @param_obj

      logger.info ("dispensary_items_save --- #{@dispensary_items_save.inspect}")

      val =  0

      @calling = @dispensary_items_save.each do | dispensed|


      while @dispensed_quantity[val] == "" do
          val = val +1
    
        end
    
          dispensed[:quantity] = @dispensed_quantity[val]
    
          val = val + 1

      end

    end
     
     
    logger.info ("dispensary_items_save 2 --- #{@dispensary_items_save.inspect}")
  
    @dispensary_items_save.each do |dispensary|

      dispense_date = Time.now.strftime("%y/%m/%d %H:%M:%S")
      @store_Item_master_Id = dispensary[:id]

      @dispensed_quantity = dispensary[:quantity]

      # logger.info "dispensary[:user_id] --- #{dispensary[:user_id]}"
      logger.info "dispensary[:quantity] --- #{dispensary[:quantity]}"
      # logger.info "dispensary[:id] --- #{dispensary[:id]}"
         
      @save_params = Dispensary.new(user_id: dispensary[:user_id], store_inventory_categories_id: dispensary[:store_inventory_categories_id], store_inventory_master_id: dispensary[:store_inventory_master_id], 
      quantity: dispensary[:quantity], comments: dispensary[:comments], status: dispensary[:status], date_dispensed: dispense_date, assigned_requests_id: dispensary[:id])

      logger.info ("save_params  --- #{@save_params.inspect}")

      respond_to do |format|
        if @save_params.save!
          format.html {  }
          format.json { render :show, status: :created, location: @dispensary }
        else
          format.html { render :new } 
          format.json { render json: @dispensary.errors, status: :unprocessable_entity }
        end
      end

    end

    #  redirect_to dispensing_path


    logger.info ("store_Item_master_Id --- #{@store_Item_master_Id}")


    @dispensary_items_save.each do | value |

     @quanty = AssignedRequest.find(value.id)
      logger.info ("Object Id --- #{@quanty.inspect}")
   
 
      #  #quantity of item in inventory master
     @item_quant = @quanty.quantity
 
     logger.info ("Assigned quantity for dispensed item -- #{@item_quant}")

     logger.info ("Dispensed quantity for assigned item -- #{@dispensed_quantity}")
 
 
      #  Performing subtraction for items assigned 
      @final_quanty = @item_quant.to_i - @dispensed_quantity.to_i 
 
      logger.info ("Quantity after subtraction --- #{@final_quanty}")
 
      #   #UPDATING DATABSE WITH NEW QUANTITY VALUE
      @answer = AssignedRequest.where(id: value.id).update(:quantity => @final_quanty )
   
      logger.info ("wHAT WE GET --- #{@answer}")


    end

    redirect_to dispensing_path

  end

  # PATCH/PUT /dispensaries/1
  # PATCH/PUT /dispensaries/1.json
  def update
    respond_to do |format|
      if @dispensary.update(dispensary_params)
        format.html { redirect_to @dispensary, notice: 'Dispensary was successfully updated.' }
        format.json { render :show, status: :ok, location: @dispensary }
      else
        format.html { render :edit }
        format.json { render json: @dispensary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dispensaries/1
  # DELETE /dispensaries/1.json
  def destroy
    @dispensary.destroy
    respond_to do |format|
      format.html { redirect_to dispensaries_url, notice: 'Dispensary was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dispensary
      @dispensary = Dispensary.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def dispensary_params
      params.require(:dispensary).permit(:assigned_requests_id, :status, :comments, :date_dispensed)
    end
end
