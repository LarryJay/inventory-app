class StoreRequestsController < ApplicationController
  before_action :set_store_request, only: [:show, :edit, :update, :destroy]
  include ActiveModel::Conversion
  extend  ActiveModel::Naming

  # GET /store_requests
  # GET /store_requests.json
  def index
    @store_requests = StoreRequest.all
    @store_inventory_categories = StoreInventoryCategory.all

    # @store_inventory_masters = StoreInventoryMaster.new
    @store_inventory_masters = StoreInventoryMaster.all.paginate(:page=>params[:page],per_page:10)


  end

@@one=2
def checkout
  @store_request = StoreRequest.new
  @store_requests = StoreRequest.all
  # @store_inventory_masters = StoreInventoryMaster.paginate(:page=>params[:page],per_page:5).order('id desc')
@store_inventory_masters = StoreInventoryMaster.limit(@@one)
# search button queries
  # @category = params[:store_inventory_master][:store_inventory_categories_id]

  # logger.info "Category selected -- #{@category}"

  @@one+=2
  respond_to do |format|
   format.html
   format.js
#   end
  end

  if params[:search]
    @store_inventory_masters = StoreInventoryMaster.where(["item_name like ?","%#{params[:search]}%"]).paginate(:page=>params[:page],per_page:5)


    puts  "name found--------hjkl "
  else
    puts "not found"

  end
end




def update_items
  @items_list = StoreInventoryMaster.where("store_inventory_categories_id = ?", params[:store_inventory_categories_id])
  respond_to do |format|
    format.js
  end
end


#creating a method for posting info back to the form from a modal
def back_to_form
@store_items_save =[]

@store_inventory_masters = StoreInventoryMaster.all
@store_request = StoreRequest.new
@checked_value = params[:check_box][:id]
@quantity_value = params[:store_request][:quantity]
@category_name = params[:store_request][:store_inventory_categories_id]

@store_manager = 1 

logger.info "Quantites passed --- #{@quantity_value.inspect}"


@checked_value.each do |values|

 @store_items = StoreInventoryMaster.find(values)

# logger.info ("---- Store Value ---- #{values.inspect}")

@lists = @store_items.inspect
# logger.info ("--STore Item OBj --- #{@lists}")

@item_name = @store_items.temp_quantity
# logger.info ("temp quantity unassigned--- #{@item_name}")

@store_items_save << @store_items

# logger.info "stor saved without temp quant value --- #{@store_items_save.inspect}"

val =  0

@calling = @store_items_save.each do |name|

  while @quantity_value[val] == "" do
    val = val +1

  end

    name[:temp_quantity]= @quantity_value[val]

  val+= 1

end

  logger.info "stor saved 2 -- #{@calling.inspect}"

end




respond_to do|format|
  format.js{render 'store_requests/form', locals: { items: @calling }}
  format.html {render 'store_requests/form', locals: { items: @calling }}
end

end


def append
  @store_request = StoreRequest.new
  store_request.item_name = params[:checkbox][:id]
  if store_request.save
    render :status => 201, :json => { id: check_box.id, quantity: quantity.quantity,item_name: item_name.name }
  else
    err_msg = store_request.errors.empty?? 'Save failed' : store_request.errors.full_messages.join('; ')
    render :status => 400, :json => { message: err_msg }
  end
end
  # GET /store_requests/1
  # GET /store_requests/1.json
  def show
  end

  # GET /store_requests/new
  def new

    @store_request = StoreRequest.new

  end



  # POST /store_requests
  # POST /store_requests.json
  def create

    #Getting values of multiple items quantity field
    @multiple_quantity = params[:quantity]

    #Getting values of multiple items id field
    @multiple_master_id = params[:store_inventory_master_id]

    @Store = params[:store_inventory_master_id]

    #Setting custom params in create method
    # @store_request = StoreRequest.new(user_id: params[:store_request][:user_id], store_inventory_categories_id: params[:store_request][:store_inventory_categories_id],
    #   comments: params[:store_request][:comments], status: params[:store_request][:status], store_manager_id: params[:store_request][:store_manager_id], date_requested: params[:store_request][:date_requested],
    #   quantity: params[:quantity], store_inventory_master_id: params[:store_inventory_master_id])

    @store_quant_val = StoreInventoryMaster.find(@multiple_master_id)

    logger.info ("Main Inventory Item Found ---- #{@store_quant_val.inspect}")

    # logger.info ("Main Inventory Item Quantity---- #{@store_quant_val.quantity}")

      val = 0; #initialising val for iteration purposes

    #Iteration through master id to save all records. Multiple items will have a unique master_id and unique quantity.
    #The number of master id is equla to the number of quantities. Iteration through any of them will work. My choice here is Master ID.
    #Set value of array master id and quantity to each record
    @multiple_master_id.each do | value |


      @save_params = StoreRequest.new(user_id: params[:store_request][:user_id], store_inventory_categories_id: params[:store_request][:store_inventory_categories_id],
      comments: params[:store_request][:comments], status: params[:store_request][:status], store_manager_id: params[:store_request][:store_manager_id], date_requested: params[:store_request][:date_requested],
      quantity: @multiple_quantity[val], store_inventory_master_id: value)

      respond_to do |format|
      if @save_params.save!
         format.html { }
        format.json { render :show, status: :created, location: @store_request }
      else
        format.html { render :new, notice:'Store request was successfully updated.' }
        format.json { render json: @store_request.errors, status: :unprocessable_entity }
      end
    end

      logger.info "Saved Params -- #{@save_params.inspect}"

      val += 1 #Iteration increment

    end

     redirect_to requests_path,notice:'Store request was successfully made.'

    # @requested_quantity = params[:store_request][:quantity]

    # logger.info ("Request quantity ---- #{@req_quantity}")
  end



  # GET /store_requests/1/edit
  def edit
  end



  # PATCH/PUT /store_requests/1
  # PATCH/PUT /store_requests/1.json
  def update
    respond_to do |format|
      if @store_request.update(store_request_params)
        format.html { redirect_to @store_request, notice: 'Store request was successfully updated.' }
        format.json { render :show, status: :ok, location: @store_request }
      else
        format.html { render :edit }
        format.json { render json: @store_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /store_requests/1
  # DELETE /store_requests/1.json
  def destroy
    @store_request.destroy
    respond_to do |format|
      format.html { redirect_to store_requests_url, notice: 'Store request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_store_request
      @store_request = StoreRequest.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def store_request_params
      params.require(:store_request).permit(:id,:user_id, :store_inventory_categories_id, :store_inventory_master_id, :quantity, :date_assigned, :store_manager_id, :comments, :status, :date_requested,:store_request_id)
    end
 end
