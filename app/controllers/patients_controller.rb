class PatientsController < ApplicationController
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
#  require './lib/push/notify.rb' 
    layout "doc_patients",:only => [ :patient_profile]
  respond_to :html, :js
  # GET /patients
  # GET /patients.json
  def index


  # notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
  # logger.info "NOTIFCATIONS --------"
  #  push =  Push::Notify.single_notification(notification_for_user,"Recptionist in Action ")
  #  logger.info push
  #  logger.info "ENDS--------"

    @patient = Patient.new
    @assign_patient = AssignPatient.new
    @reception_appointment = Appointment.new
    #@patients = Patient.all.paginate(page: params[:page], per_page: params[:count]).order('created_at desc')
    @patients = Patient.paginate(:page => params[:page], :per_page => 10).order('created_at desc') 


    #GENERATING PATIENTS ID
    # @get_all_patient_id = Patient.all.order('id desc') 
    # @patients_bid  = @get_all_patient_id[0].id
    # logger.info "PATIENTS ID #{@patients_bid}"
    
    # @bmc_id = @patients_bid + 1
    # logger.info "BMC ID #{@bmc_id}"

    # @patient_bmc = patient_number.to_s + @bmc_id.to_s

    # logger.info "@patient_bmc #{@patient_bmc}" 
    ############################################

    if params[:count]
      params[:count]
    else
      params[:count] = 20 
    end

    if params[:page]
      page = params[:page].to_i
    else
    page = 1
    end

    if params[:per_page].present?
      perpage = params[:per_page]
    else
    perpage = 20
    end

    @per_page = params[:per_page] || Patient.per_page || 20

    page = if params[:page]
      params[:page].to_i
    else
    1
    end

    per_page = 20
    offset = (page - 1) * per_page
    limit = page * per_page
    @array = *(offset...limit)

    if params[:search_value] && params[:search_value].strip != ''
      if params[:search_param] == 'card_no'
        @patients = Patient.patient_no_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
      elsif params[:search_param] == 'mobile_number'
        @patients = Patient.mobile_number_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
      elsif params[:search_param] == 'name'
        @patients = Patient.name_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
      elsif params[:search_param] == 'f_name'
        @patients = Patient.f_name_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
      end

      
    elsif params[:search_param] == 'date'
      start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
      ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"

      @patients = Patient.date_search(start,ended).paginate(page: params[:page], per_page: params[:count]).order('created_at asc')

    else
    end
  end
    
   def patient_profile

    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

    logger.info "Lets see patient_id id #{@patient_id}"
    logger.info "Lets see  Patient name #{@patient_records.surname } #{@patient_records.other_names }"


     @basic_med_records = BasicMedRecord.joins("INNER JOIN patients ON patients.id = basic_med_records.patient_id
                    ").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,weight,height,saturation,occupation,address,dob,patient_id,basic_med_records.admission_status,basic_med_records.id,basic_med_records.created_at,card_no').where(patient_id: patient_id).order('id desc')

      logger.info "PATIENT RECORD #{@basic_med_records[0].inspect}"
   end
  
  

  def patient_number
    @get_all_patient_id = Patient.all.order('id desc') 

    #.exists?
    logger.info "PATIENTS ID In random function #{@get_all_patient_id}"
     time=Time.new
    strtm = time.strftime("BMC"+"%y%m"+"0000")
    logger.info "strtm #{strtm}"

    if @get_all_patient_id.exists?
      
       @patients_bid  = @get_all_patient_id[0].id
    logger.info "OLD ID #{@patients_bid}"

    @bmc_id = @patients_bid + 1
    logger.info "BMC ID #{@bmc_id}"  
    
    @patient_bmc = strtm.to_s + @bmc_id.to_s
    logger.info "@patient_bmc #{@patient_bmc}" 
    return @patient_bmc
    else
    logger.info "ID IS NIL"
    @patient_bmc = strtm = time.strftime("BMC"+"%y%m"+"00001")
    logger.info "@patient_bmc #{@patient_bmc}" 
    return @patient_bmc
    end
   

    

    
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
  end

  # GET /patients/new
  def new

    @patient = Patient.new

    patient_id=params[:id]

    @patients1 = Patient.find_by(id: patient_id)
    logger.info "patients1: #{@patients1}"
    @patients1.other_names
    logger.info "other_names: #{@patients1.other_names}"

  end

  ########### PARTIALS ###############

  def patients_form
    @patient_number = patient_number
    logger.info "Patient BMID is #{@patient_number}"

    @patient = Patient.new

    respond_to do |format|

      format.html
      format.js
    end
  end

  def show_patients
    patient_id=params[:id]

    @patients1 = Patient.find_by(id: patient_id)
    @bill_id = BillHistory.where(patient_id: patient_id).order('id desc')

    # @retrieve_last_doc = AssignPatient.where(patient_id: patient_id, personnel_id: 1 ).order('id desc')
     @retrieve_last_doc =  AssignPatient.joins("INNER JOIN users ON users.id = assign_patients.personnel_id
                     ").select('assign_patients.id,fullname').where(patient_id: patient_id ).order('id desc')


    @doc_name  = @retrieve_last_doc[0].fullname
    logger.info "Las DOctor: #{@doc_name}"

    @bill_update_at  = @bill_id[0].updated_at

    logger.info "patients1: #{@patients1}"
    @create_at = @patients1.created_at
    @update_at = @patients1.updated_at

    @hour_diff = @bill_update_at.hour-@update_at.hour

    @min_diff = @bill_update_at.min-@update_at.min

    retrieve_dob = @patients1.dob
    now = Time.now.utc.to_date

    logger.info "now IS #{now.year} dob #{retrieve_dob.year}"
    @age = now.year - retrieve_dob.year
    logger.info "AGE IS #{@age}"

    logger.info "Bill Updated AT : #{@bill_update_at}"
    logger.info "create_at & update_at : #{@create_at} #{@update_at}"
    logger.info "hour_diff : #{@hour_diff}"
    logger.info "min_diff : #{@min_diff}"
        
  end


  def edit_patients
    patient_id=params[:id]
    @patient = Patient.new

    @patients1 = Patient.find_by(id: patient_id)
    logger.info "patients1: #{@patients1}"
    @patients1.other_names
    logger.info "other_names: #{@patients1.other_names}"
    logger.info "ID: #{@patients1.id}"

  end

  def assign_a_patient
    @assign_patient = AssignPatient.new
    patient_id=params[:id]

    logger.info "Lets see id para #{patient_id}"

    @patient = Patient.new
    @records = Patient.find_by(id: patient_id)

    logger.info "Lets see the records"
    logger.info "record: #{@records.inspect}"

    @patient_id = @records.id
    patient_id = @patient_id
    logger.info "patient id: #{@patient_id}"

    @check_insurance = @records.insurance_cash
    @check_card = @records.card_no
    logger.info "patient CASH OR INSURANCE: #{@check_insurance}"
     logger.info "patient card_no: #{@check_card}"

    

    respond_to do |format|

      format.html
      format.js
    end

  end

  def reception_patient
    @reception_appointment = Appointment.new
    @patient_id=params[:id]

    logger.info "Lets see id of patient in Appointment #{@patient_id}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  # GET /patients/1/edit
  def edit
    patient_id=params[:id]

    @patient = Patient.find(params[:id])
  end

  # POST /patients
  # POST /patients.json
  def create
      service_id =  1027  #Registration service ID
      @services_data = ServicesInventory.find_by(id: service_id)
      price = @services_data.price


    @get_patient_id = Patient.all.order('id desc') 
    if @get_patient_id.exists?
      
    @retrive_old_idd  = @get_patient_id[0].id
    logger.info "OLD ID #{@retrive_old_idd}"

    @new_p_id = @retrive_old_idd + 1
    logger.info "NEW P ID #{@new_p_id}" 

     @bills = BillHistory.new({:service_id => service_id,:patient_id => @new_p_id, :price => price ,:status=> 0,:delete_status => 0})
      @bills_temp = BillTemp.new({:patient_id => @new_p_id, :status=> 0})
      @bills.save
      @bills_temp.save 

    else
    logger.info "ID IS NIL"
     @new_p_id =  1
    logger.info "FRESH ID #{@new_p_id}" 

     @bills = BillHistory.new({:service_id => service_id,:patient_id => @new_p_id, :price => price ,:status=> 0,:delete_status => 0})
      @bills_temp = BillTemp.new({:patient_id => @new_p_id, :status=> 0})
      @bills.save
      @bills_temp.save 
    
    end
  
    @patient = Patient.new(patient_params)
   

        #NOTIFICATION TO CASHIER
    # notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
    # logger.info "NOTIFCATIONS --------"
    #  push =  Push::Notify.single_notification(notification_for_user,"NEW BILL GENERATED FOR REGISTRATION")
    #  logger.info push
    #  logger.info "ENDS--------"
   

    respond_to do |format|
      if @patient.save
        logger.info "PATIENT SAVE #{@patient.save}"
        format.html { redirect_to patients_path, notice: 'Patient Record was successfully created.' }
        format.json { render :show, status: :created, location: @patient }
      else
        format.html { render :new }
        #format.html { redirect_to patients_form_path, notice: 'Check fields.' }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    id=params[:id]
    respond_to do |format|
      id=params[:id]
      if @patient.update(patient_params)
        # format.html { redirect_to @patient, notice: 'Patient was successfully updated.' }
        format.html { redirect_to patients_path, notice: 'Patient was successfully updated.' }
        format.json { render :show, status: :ok, location: @patient }
      else
        format.html { render :edit }
        format.json { render json: @patients1.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    respond_to do |format|
      format.html { redirect_to patients_url, notice: 'Patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_patient
    @patient = Patient.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def patient_params
    params.require(:patient).permit(:email,:marital_status,:emergency_c_email,:emergency_c_location,:assigned_status,:insurance_cash,:refers,:gender, :religion,:emergency_c_name,:emergency_c_number,:id,:title, :surname, :other_names, :mobile_number, :occupation, :address, :status, :user_id, :alt_number, :dob,:card_no)
  end
end
