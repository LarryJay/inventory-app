class StoreInventoryCategoriesController < ApplicationController
  before_action :set_store_inventory_category, only: [:show, :edit, :update, :destroy]

  # GET /store_inventory_categories
  # GET /store_inventory_categories.json
  def index
    @store_inventory_categories = StoreInventoryCategory.all
  end

  # GET /store_inventory_categories/1
  # GET /store_inventory_categories/1.json
  def show
  end

  # GET /store_inventory_categories/new
  def new
    @store_inventory_category = StoreInventoryCategory.new
  end

  # GET /store_inventory_categories/1/edit
  def edit
  end

  # POST /store_inventory_categories
  # POST /store_inventory_categories.json
  def create
    @store_inventory_category = StoreInventoryCategory.new(store_inventory_category_params)

    respond_to do |format|
      if @store_inventory_category.save
        format.html { redirect_to store_inventory_categories_path, notice: 'Store category was successfully created.' }
        format.json { render :show, status: :created, location: @store_inventory_category }
      else
        format.html { render :new }
        format.json { render json: @store_inventory_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /store_inventory_categories/1
  # PATCH/PUT /store_inventory_categories/1.json
  def update
    respond_to do |format|
      if @store_inventory_category.update(store_inventory_category_params)
        format.html { redirect_to @store_inventory_category, notice: 'Store inventory category was successfully updated.' }
        format.json { render :show, status: :ok, location: @store_inventory_category }
      else
        format.html { render :edit }
        format.json { render json: @store_inventory_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /store_inventory_categories/1
  # DELETE /store_inventory_categories/1.json
  def destroy
    @store_inventory_category.destroy
    respond_to do |format|
      format.html { redirect_to store_inventory_categories_url, notice: 'Store inventory category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_store_inventory_category
      @store_inventory_category = StoreInventoryCategory.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def store_inventory_category_params
      params.require(:store_inventory_category).permit(:name, :status)
    end
end
