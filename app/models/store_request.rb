class StoreRequest < ApplicationRecord
    
    validates :quantity, presence: true, inclusion: 1...Float::INFINITY
    validates :user_id, presence: true, inclusion: 1...Float::INFINITY
    validates :store_manager_id, presence: true, inclusion: 1...Float::INFINITY
    validates :store_inventory_categories_id, presence: true, inclusion: 1...Float::INFINITY
    validates :store_inventory_master_id, presence: true, inclusion: 1...Float::INFINITY
    validates :comments, presence: true
    validates :date_requested, presence: true

end
