class StoreInventoryMaster < ApplicationRecord
    validates :quantity, presence: true, inclusion: 1...Float::INFINITY
    validates :store_inventory_categories_id, presence: true, inclusion: 1...Float::INFINITY
    validates :item_name, presence: true, uniqueness: true
    validates :cost_price, presence: true, inclusion: 0...Float::INFINITY
    validates :selling_price, presence: true, inclusion: 0...Float::INFINITY
    validates :manufaturer_name, presence: true
    validates :user_id, presence: true, inclusion: 1...Float::INFINITY

end
