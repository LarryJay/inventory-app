json.extract! lab_request, :id, :basic_medical_record_id, :patient_id, :user_id, :personnel_id, :status, :lab_type, :created_at, :updated_at
json.url lab_request_url(lab_request, format: :json)
