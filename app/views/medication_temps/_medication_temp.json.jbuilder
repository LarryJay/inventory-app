json.extract! medication_temp, :id, :patient_id, :service_id, :status, :created_at, :updated_at
json.url medication_temp_url(medication_temp, format: :json)
