json.extract! services_inventory, :id, :service_name, :price, :category_id, :quantifiable, :created_at, :updated_at
json.url services_inventory_url(services_inventory, format: :json)
