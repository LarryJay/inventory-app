json.extract! treatment_sheet, :id, :drugs, :user_id, :time, :status, :created_at, :updated_at
json.url treatment_sheet_url(treatment_sheet, format: :json)
