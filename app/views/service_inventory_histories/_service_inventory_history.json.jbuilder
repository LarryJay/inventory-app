json.extract! service_inventory_history, :id, :service_id, :price, :created_at, :updated_at
json.url service_inventory_history_url(service_inventory_history, format: :json)
