require 'test_helper'

class OrderMastersControllerTest < ActionController::TestCase
  setup do
    @order_master = order_masters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:order_masters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create order_master" do
    assert_difference('OrderMaster.count') do
      post :create, order_master: { order_name: @order_master.order_name, status: @order_master.status }
    end

    assert_redirected_to order_master_path(assigns(:order_master))
  end

  test "should show order_master" do
    get :show, id: @order_master
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @order_master
    assert_response :success
  end

  test "should update order_master" do
    patch :update, id: @order_master, order_master: { order_name: @order_master.order_name, status: @order_master.status }
    assert_redirected_to order_master_path(assigns(:order_master))
  end

  test "should destroy order_master" do
    assert_difference('OrderMaster.count', -1) do
      delete :destroy, id: @order_master
    end

    assert_redirected_to order_masters_path
  end
end
