require 'test_helper'

class RadiologyRequestsControllerTest < ActionController::TestCase
  setup do
    @radiology_request = radiology_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:radiology_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create radiology_request" do
    assert_difference('RadiologyRequest.count') do
      post :create, radiology_request: { basic_medical_record_id: @radiology_request.basic_medical_record_id, patient_id: @radiology_request.patient_id, personnel_id: @radiology_request.personnel_id, radiology: @radiology_request.radiology, status: @radiology_request.status, user_id: @radiology_request.user_id }
    end

    assert_redirected_to radiology_request_path(assigns(:radiology_request))
  end

  test "should show radiology_request" do
    get :show, id: @radiology_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @radiology_request
    assert_response :success
  end

  test "should update radiology_request" do
    patch :update, id: @radiology_request, radiology_request: { basic_medical_record_id: @radiology_request.basic_medical_record_id, patient_id: @radiology_request.patient_id, personnel_id: @radiology_request.personnel_id, radiology: @radiology_request.radiology, status: @radiology_request.status, user_id: @radiology_request.user_id }
    assert_redirected_to radiology_request_path(assigns(:radiology_request))
  end

  test "should destroy radiology_request" do
    assert_difference('RadiologyRequest.count', -1) do
      delete :destroy, id: @radiology_request
    end

    assert_redirected_to radiology_requests_path
  end
end
