require 'test_helper'

class ServiceInventoryHistoriesControllerTest < ActionController::TestCase
  setup do
    @service_inventory_history = service_inventory_histories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:service_inventory_histories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create service_inventory_history" do
    assert_difference('ServiceInventoryHistory.count') do
      post :create, service_inventory_history: { price: @service_inventory_history.price, service_id: @service_inventory_history.service_id }
    end

    assert_redirected_to service_inventory_history_path(assigns(:service_inventory_history))
  end

  test "should show service_inventory_history" do
    get :show, id: @service_inventory_history
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @service_inventory_history
    assert_response :success
  end

  test "should update service_inventory_history" do
    patch :update, id: @service_inventory_history, service_inventory_history: { price: @service_inventory_history.price, service_id: @service_inventory_history.service_id }
    assert_redirected_to service_inventory_history_path(assigns(:service_inventory_history))
  end

  test "should destroy service_inventory_history" do
    assert_difference('ServiceInventoryHistory.count', -1) do
      delete :destroy, id: @service_inventory_history
    end

    assert_redirected_to service_inventory_histories_path
  end
end
